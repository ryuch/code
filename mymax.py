import sys, os
import time, datetime
import random

# The screen clear function
def screen_clear():
    # for mac and linux(here, os.name is 'posix')
    if os.name == 'posix':
        _ = os.system('clear')
    else:
        # for windows platfrom
        _ = os.system('cls')

# print numbers in list
def draw_num(a):
    for i in a:
        print(i, end=' ')

# search the max number
def mymax(a):
    max = -1
    min = 100
    for i in range(len(a)):
        if a[i]>max :
            max = a[i]
        if a[i]<min :
            min = a[i]

    return max, min

# get the random a numbers
def rand_num(a):
    l = []
    for x in range(int(a/10)):
        l.append(random.randint(0,a))
    return l

def game(n):

    nlist = rand_num(n)
    max,min = mymax(nlist)
    ttime = 0
    n_failed = 0

    while 1:
        tstart = time.time()
        screen_clear()
        draw_num(nlist)
        input("\n--- Press enter when you found the max ---")
        tend = time.time()
        ttime = tend - tstart + ttime
        screen_clear()
        usermax = input("What is it?:")
        if len(usermax) == 0: usermax = 0
        if max == int(usermax):
            print("You are right!")
            break
        else:
            print("Your are not right!")
            n_failed = n_failed +1
    telapsed = round(ttime*10)/10
#    print("Time elapsed:", telapsed)
    return telapsed, n_failed

#        draw_num(nlist)

glen = int(sys.argv[1])
ngame = int(sys.argv[2])

gtime = 0
t_failed = 0
t_games = []

for i in range(ngame):
    ltime, n_failed = game(glen)
    t_failed = t_failed + n_failed
    gtime = gtime + ltime
    t_games.append(ltime)

avg_time = round(100*gtime/ngame)/100

print("Number of wrong guess:", t_failed, "of", t_failed+ngame)
print("Avg time:",avg_time)
print("Time for each games:",t_games)

f = open("max.score", "a+")

ctime =  datetime.datetime.now().strftime("%x %X")
name = sys.argv[3]
f.write(str(avg_time)+" "+name+" N="+str(glen)+" "+ctime+"\n")
f.close()
